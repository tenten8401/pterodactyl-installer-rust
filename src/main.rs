use exitcode;
use gethostname;
use os_info;
use path_clean::{clean, PathClean};
use regex::Regex;
use std::env;
use std::{io, io::Write, path::Path, path::PathBuf, process};
use users::get_current_uid;
mod dependencies;

fn preflight(info: &os_info::Info) -> bool {
    // Run checks
    let supported_systems = [
        os_info::Info::new(os_info::Type::Fedora, os_info::Version::custom("30", None)),
        os_info::Info::new(os_info::Type::Fedora, os_info::Version::custom("29", None)),
        os_info::Info::new(
            os_info::Type::Ubuntu,
            os_info::Version::custom("18.04", None),
        ),
    ];
    if supported_systems.contains(&info) {
        if get_current_uid() != 0 {
            println!("You must be root to run this installer!");
            false
        } else {
            true
        }
    } else {
        // Display list of supported systems
        let mut systems_list = String::new();
        for system in supported_systems.iter() {
            systems_list.push_str(
                &format!(
                    "- {distro} {version}\n",
                    distro = system.os_type(),
                    version = system.version()
                )[..],
            );
        }
        println!(
            "This operating system is not supported! Please install one of the following:\n{}",
            systems_list
        );
        false
    }
}

fn prompt(msg: String) -> String {
    let mut ans = String::new();
    print!("{}", msg);
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut ans)
        .expect("Failed to read line!");
    ans = ans.trim().to_string();
    return ans;
}

fn clear_term() {
    print!("{}[2J", 27 as char);
    io::stdout().flush().unwrap();
}

fn ask_options() -> i8 {
    loop {
        // Clear terminal & print greeting
        clear_term();
        println!("Welcome to Isaac's Pterodactyl Installer!");
        println!("");
        println!("[1] Install the panel + daemon (complete install)");
        println!("[2] Install the daemon");
        println!("[3] Install the panel");
        println!("[4] Update both (if installed)");
        println!("");
        let selection = prompt("Please choose an option: ".to_string());

        let selection: i8 = match selection.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        return selection;
    }
}

fn install_panel(info: &os_info::Info) {
    let yes_regex = Regex::new("[Yy]").unwrap();
    let default_install_path = "/var/www/pterodactyl/";
    let default_url = gethostname::gethostname().into_string().unwrap();

    clear_term();
    println!("Default URL: {}", default_url);
    // Prompt for info
    let mut install_path = PathBuf::new();
    loop {
        install_path = loop {
            let mut install_path_str = String::new();
            let mut install_path_tmp = PathBuf::from(default_install_path).clean();
            println!("Default Hostname: {:?}", default_url);
            install_path_str = prompt(format!(
                "Installation Path [{default_install_path}]: ",
                default_install_path = install_path_tmp.to_str().unwrap(),
            ));

            if !install_path_str.is_empty() {
                install_path_tmp = env::current_dir()
                    .unwrap()
                    .join(PathBuf::from(install_path_str))
                    .clean();
            }

            if !install_path_tmp.exists() {
                let create_path = prompt(format!(
                    "{install_path} does not exist, would you like to create it [Y/n]? ",
                    install_path = install_path_tmp.to_str().unwrap()
                ));
                if !yes_regex.is_match(&create_path[..]) {
                    continue;
                }
                std::fs::create_dir_all(&install_path_tmp).expect("Could not create directory!");
            }
            break install_path_tmp;
        };

        loop {
            break;
        }

        break;
    }

    // Install PHP
    println!("Installing dependencies...");
    if !dependencies::install_panel_deps(&info) {
        println!("Dependency installation failed!");
        process::exit(exitcode::TEMPFAIL);
    }
}

fn main() {
    let yes_regex = Regex::new("[Yy]").unwrap();
    let info = os_info::get();

    if preflight(&info) {
        // Ask user if OS install is fresh
        let ans = prompt(format!(
            "\nYou will need a fresh install of {os_type} to continue.\nIs this a fresh install [y/N]? ",
            os_type = info.os_type()
        ));
        if yes_regex.is_match(&ans) {
            // Prompt
            let selection = ask_options();
            if selection == 1 {
                install_panel(&info);
            }
        } else {
            println!("Please run this on a fresh install and try again.");
            process::exit(exitcode::DATAERR);
        }

        process::exit(exitcode::OK);
    } else {
        process::exit(exitcode::DATAERR);
    }
}
