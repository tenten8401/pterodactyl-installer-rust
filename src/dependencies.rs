use os_info;
use std::process::Command;

pub fn install_panel_deps(info: &os_info::Info) -> bool {
    return match info.os_type() {
        os_info::Type::Fedora => {
            // Fedora needs no repos
            let deps = [
                // PHP
                "php",
                "php-cli",
                "php-gd",
                "php-pdo",
                "php-mbstring",
                "php-tokenizer",
                "php-bcmath",
                "php-xml",
                "php-fpm",
                "php-curl",
                "php-zip",
                "php-json",
                "php-mysqlnd",
                // Redis
                "redis",
                // MariaDB
                "mariadb",
                "mariadb-server",
                // Nginx
                "nginx",
                // Misc
                "certbot-nginx",
                "curl",
                "tar",
                "unzip",
                "git",
                "policycoreutils-python-utils",
            ];
            if !install_packages(&info, &deps) {
                return false;
            }
            true
        }
        os_info::Type::Ubuntu => {
            let repos = ["ppa:ondrej/php", "ppa:chris-lea/redis-server", "universe"];
            let deps = [
                // PHP
                "php7.2",
                "php7.2-cli",
                "php7.2-gd",
                "php7.2-mysql",
                "php7.2-pdo",
                "php7.2-mbstring",
                "php7.2-tokenizer",
                "php7.2-bcmath",
                "php7.2-xml",
                "php7.2-fpm",
                "php7.2-curl",
                "php7.2-zip",
                // MariaDB
                "mariadb-server",
                // Nginx
                "nginx",
                // Redis
                "redis-server",
                // Misc
                "tar",
                "unzip",
                "git",
            ];
            if !add_repos(&info, &repos) {
                return false;
            }
            if !install_packages(&info, &deps) {
                return false;
            }
            true
        }
        _ => false,
    };
}

pub fn install_packages(info: &os_info::Info, pkgs: &[&str]) -> bool {
    // Should cover all supported operating systems
    match info.os_type() {
        os_info::Type::Fedora => {
            let install = Command::new("dnf")
                .arg("install")
                .args(pkgs)
                .arg("-y")
                .status()
                .expect("Failed to execute command!");
            return install.success();
        }
        os_info::Type::Ubuntu => {
            let update = Command::new("apt")
                .arg("update")
                .status()
                .expect("Failed to execute command!");
            if update.success() {
                let install = Command::new("apt")
                    .arg("install")
                    .args(pkgs)
                    .arg("-y")
                    .status()
                    .expect("Failed to execute command!");
                return install.success();
            } else {
                return false;
            }
        }
        _ => panic!("wtf"),
    };
}

pub fn add_repos(info: &os_info::Info, repos: &[&str]) -> bool {
    match info.os_type() {
        os_info::Type::Ubuntu => {
            install_packages(&info, &["software-properties-common"]);
            for repo in repos {
                let addrepo = Command::new("add-apt-repository")
                    .arg(repo)
                    .arg("-y")
                    .status()
                    .expect("Failed to execute command!");
                if !addrepo.success() {
                    return false;
                }
            }
            return true;
        }
        _ => panic!("wtf"),
    }
}
